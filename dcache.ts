
async function main(args: string[]): Promise<number> {
  const proc = Deno.run({
    cmd: [ "deno", "cache", ...args ],
  })

  const status = await proc.status()

  return status.code
}

Deno.exit(await main(Deno.args))
